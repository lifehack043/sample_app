require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @base_title = "Ruby on Rails Tutorial Sample App"
  end
  test "should get home" do
    get root_path
    assert_response :success
    assert_select "title", "Главная | Твои новости"
  end
  test "should get help" do
    get help_path
    assert_response :success
    assert_select "title", "Помощь | Твои новости"
  end
end