Rails.application.routes.draw do
 
  resources :records
  get 'search/index'

  get 'password_resets/new'

  get 'password_resets/edit'

  root 'static_pages#home'
  get '/reactJS',     to: 'static_pages#reactJS'
  get '/signup',   to: 'users#new'
  post '/signup',  to: 'users#create'
  get '/login',    to: 'sessions#new'
  post '/login',   to: 'sessions#create'  
  delete '/logout',to: 'sessions#destroy'
  get 'results', to: 'users#index', as: 'results'
  get '/auth/vkontakte/callback', to: 'omniauth#create'
  resources :users do
    member do
      get :following, :followers
    end
  end
  
  resources :microposts do
    member do
      get :like, :unlike
    end
  end
  
 
  #,          only: [:create, :destroy]
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :relationships,       only: [:create, :destroy]
end
