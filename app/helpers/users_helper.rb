module UsersHelper
  def gravatar_for(user, options = { size: 80} )
      size = options[:size]
    if user.avatar?
      case size
        when 80
          gravatar_url = user.avatar.medium.url
        when 100
          gravatar_url = user.avatar.large.url
        when 50
          gravatar_url = user.avatar.thumb.url
        when 30
          gravatar_url = user.avatar.low.url
        when 150
          gravatar_url = user.avatar.url
        end
      elsif user.email.nil?
        gravatar_url = "placehold.png"
      else
        gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
        gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    end
    return gravatar_url
  end
  
  def avatars_url(users)
    arr = []
    users.each do |user|
    arr << {id: user.id, name: user.name, avatar: gravatar_for(user, size: 150)}
    end
    return arr.to_json
  end
end
