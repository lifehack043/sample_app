import PropTypes from 'prop-types'


const options = {
    date: "SORTED_BY_DATE",
    title: "SORTED_BY_TITLE",
    rating: "SORTED_BY_RATING"
}



const SortMenu = ({ sort="SORTED_BY_DATE", onSelect=f=>f}) =>
    <nav className="menu">
        <h1>Органайзер цветов</h1>
        <select className="col-md-1">
        {Object.keys(options).map((item, i) =>
            <option key={i}
               href="#"
               className={(sort === options[item]) ? "selected" : null}
               onClick={e => {
                   e.preventDefault()
                   onSelect(options[item])
               }}>{item}
            </option>
        )}
        </select>
    </nav>


SortMenu.propTypes = {
    sort: PropTypes.string,
    onSelect: PropTypes.func
}


export default SortMenu